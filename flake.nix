{
  description = "Στιχολόγιο και συλλογή εκτελέσεων ρεμπέτικων (και όχι μόνο) τραγουδιών";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";

  outputs = { self, nixpkgs }: {
    defaultPackage.x86_64-linux =
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation rec {
        name = "stichologio";
        src = self;
        nativeBuildInputs = [
          (texlive.combine {
            inherit (texlive)
              scheme-minimal

              # LaTeX packages
              colortbl
              etoolbox
              fontsize
              fontspec
              graphics
              hyperref
              infwarerr
              kvoptions
              parskip
              pdftexcmds
              preprint
              realscripts
              relsize
              tools
              xcolor
              xetex
              xkeyval
              xltxtra
              xunicode

              # Fonts
              gfsneohellenic

              # Build tools
              latex
              latexmk
              ;
          })
        ];

        FONTCONFIG_FILE = makeFontsConf { fontDirectories = texlive.gfsneohellenic.pkgs; };

        buildPhase = "latexmk -f- -xelatex -outdir=output -interaction=nonstopmode -synctex=1 main.tex";
        installPhase = ''
          mkdir $out
          cp output/main.pdf $out/${name}.pdf
        '';
      };
  };
}
